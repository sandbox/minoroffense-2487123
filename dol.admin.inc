<?php
/**
 * Administration forms for dol
 */

/**
 * Configuration form for digitalocean library
 */
function dol_admin_form($form, &$form_state) {
  $form['dol_default_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Default Token'),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => variable_get('dol_default_token'),
  );

  return system_settings_form($form);
}