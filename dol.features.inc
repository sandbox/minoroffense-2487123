<?php
/**
 * @file
 * dol.features.inc
 */

/**
 * Implements hook_default_wsconfig().
 */
function dol_default_wsconfig() {
  $items = array();
  $items['dol_droplets'] = entity_import('wsconfig', '{
    "connector" : {},
    "type" : "digitalocean_v2",
    "language" : "",
    "name" : "dol_droplets",
    "title" : "DigitalOcean Droplets",
    "created" : "1431407545",
    "changed" : "1431408194",
    "data" : {
      "cache_default_time" : 10,
      "cache_default_override" : 0,
      "create_data_method" : "droplets",
      "options" : {
        "create_data_method" : { "headers" : { "Content-Type" : "application\\/json" } },
        "read_data_method" : { "headers" : { "Content-Type" : "application\\/json" } },
        "index_data_method" : { "headers" : { "Content-Type" : "application\\/json" } }
      },
      "read_data_method" : "droplets\\/%id",
      "index_data_method" : "droplets"
    },
    "wsconfig_type" : {"type":"digitalocean_v2","label":"DigitalOcean V2","id":"4","weight":"0","data":{"endpoint":"https:\\/\\/api.digitalocean.com\\/v2","connector":"restclient_wsconnector","language plugin":"default","language always":0},"status":"1","module":null}
  }');
  $items['dol_sizes'] = entity_import('wsconfig', '{
    "connector" : {},
    "type" : "digitalocean_v2",
    "language" : "",
    "name" : "dol_sizes",
    "title" : "DigitalOcean Sizes",
    "created" : "1431408053",
    "changed" : "1431408204",
    "data" : {
      "cache_default_time" : 30,
      "cache_default_override" : 0,
      "read_data_method" : "sizes\\/%id",
      "options" : {
        "read_data_method" : { "headers" : { "Content-Type" : "application\\/json" } },
        "index_data_method" : { "headers" : { "Content-Type" : "application\\/json" } }
      },
      "index_data_method" : "sizes"
    },
    "wsconfig_type" : {"type":"digitalocean_v2","label":"DigitalOcean V2","id":"4","weight":"0","data":{"endpoint":"https:\\/\\/api.digitalocean.com\\/v2","connector":"restclient_wsconnector","language plugin":"default","language always":0},"status":"1","module":null}
  }');
  return $items;
}

/**
 * Implements hook_default_wsconfig_type().
 */
function dol_default_wsconfig_type() {
  $items = array();
  $items['digitalocean_v2'] = entity_import('wsconfig_type', '{
    "type" : "digitalocean_v2",
    "label" : "DigitalOcean V2",
    "weight" : "0",
    "data" : {
      "endpoint" : "https:\\/\\/api.digitalocean.com\\/v2",
      "connector" : "restclient_wsconnector",
      "language plugin" : "default",
      "language always" : 0
    }
  }');
  return $items;
}
